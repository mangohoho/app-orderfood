package inu.doan.androideatit.Model;

public class categories {
    private String Name;
    private String Image;

    public categories() {
    }

    public categories(String name, String image) {
        Name = name;
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
